"///////////////////////////////////////////////////////////////////////////////
" Format Paragraph
"
" Reference:
"     http://brainacle.com/how-to-write-vim-plugins-with-python.html
"
" Copyright (c) 2013 Tom Huang Software Consultancy
" All rights reserved.
"///////////////////////////////////////////////////////////////////////////////

function! Reddit()

python3 << EOF

import vim
import re


def get_char_width(char):
    if len(char) == 0:
        return 0

    if ord(char) > 255:
        return 2
    else:
        return 1


def format_current_para():
    r_blankLine = re.compile("^\s*$")
    LINE_WIDTH = 80
    
    try:
        if re.match(r_blankLine, vim.current.line) is not None:
            return
    
        current_row = vim.current.window.cursor[0]
        current_index = current_row - 1
        
        # search the starting row of the current paragraph
        start_row = 0
        if vim.current.window.cursor[0] == 1:
            start_row = 1
        else:
            # scan from the previous line to the start of the file
            for i in range(current_index - 1, -1, -1):
                line = vim.current.window.buffer[i]
                if re.match(r_blankLine, line) is not None:
                    start_row = i + 2
                    break
                elif i == 0:
                    start_row = 1
                else:
                    continue
    
        # search the starting row of the current paragraph
        end_row = 0
        if vim.current.window.cursor[0] == len(vim.current.buffer):
            end_row = len(vim.current.buffer)
        else:
            # scan from the next line to the end of the file
            for i in range(current_index + 1, len(vim.current.buffer)):
                line = vim.current.window.buffer[i]
                if re.match(r_blankLine, line) is not None:
                    end_row = i
                    break
                elif i == len(vim.current.buffer) - 1:
                    end_row = len(vim.current.buffer)
                else:
                    continue
    
        # print("(%d, %d)" % (start_row, end_row))

        start_row_index = start_row - 1
        end_row_index = end_row - 1
        para_str = u""
        for line in vim.current.buffer[start_row_index : end_row_index + 1]:
            uni_str = line #line.decode("utf-8")i
            if len(para_str) != 0 and get_char_width(para_str[-1]) != get_char_width(uni_str[0]):
                para_str += u' ' + uni_str
            else:
                para_str += uni_str

        new_buf = []
        new_str = u""
        tmp = u""
        tmp_width = 0
        last = u""
        char_width = 0
        pos = 0
        for c in para_str:
            char_width = get_char_width(c)
            if pos + tmp_width > LINE_WIDTH:
                # print "pos + tmp_width = " + str(pos + tmp_width)
                new_buf.append(new_str)
                new_str = tmp.lstrip()
                pos = tmp_width
                tmp = u""
                tmp_width = 0
            elif char_width == 2 and get_char_width(last) == 2:
                new_str += tmp
                pos += tmp_width
                tmp = u""
                tmp_width = 0
            elif char_width == 2 and get_char_width(last) == 1 and not last.isspace():
                new_str += tmp
                pos += tmp_width
                tmp = u""
                tmp_width = 0
            elif char_width == 1 and get_char_width(last) == 2:
                new_str += tmp
                pos += tmp_width
                tmp = u""
                tmp_width = 0
            elif c.isspace() and get_char_width(last) == 1 and not last.isspace():
                new_str += tmp
                pos += tmp_width
                tmp = u""
                tmp_width = 0

            tmp += c
            tmp_width += char_width
            last = c
            # print u"tmp       = " + tmp
            # print u"tmp_width = " + str(tmp_width)
            # print u"last      = " + last
            # print u"new_str   = " + new_str

        if pos + char_width > LINE_WIDTH:
            new_buf.append(new_str)
            new_buf.append(tmp.lstrip())
        else:
            new_str += tmp
            new_buf.append(new_str)
        # print "new_buf count = " + str(len(new_buf))
        # for i in range(0, len(new_buf)):
        #     print "new_buf[%d] = %s" % (i, new_buf[i])

        del vim.current.buffer[start_row_index : end_row_index + 1]
        for i in range(start_row_index, start_row_index + len(new_buf)):
            # print "i = " + str(i)
            encoded_line = new_buf[i - start_row_index].encode("utf-8")
            # print "encoded_line = " + encoded_line
            vim.current.buffer.append(encoded_line, i)
    
    except Exception as e:
        print(e)


format_current_para()

EOF

endfunction

