set nocompatible
source $VIMRUNTIME/vimrc_example.vim
source $VIMRUNTIME/mswin.vim
behave mswin

colorscheme solarized
set enc=utf-8
set guifont=Cascadia_Mono_PL:h11
set guifontwide=DFKai-SB:h12.5
set nobackup
set noundofile
set noswapfile
set smarttab
set autoindent
set ts=4
set sw=4
set expandtab
set lines=40
set columns=120
set nu

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('$VIM/vimfiles/plugged')

" Make sure you use single quotes

Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-fugitive'
Plug 'Chiel92/vim-autoformat'
" Initialize plugin system
call plug#end()

map <C-n> :NERDTreeToggle<CR>

"let &pythonthreedll = $VIM.'\python-3.6.8\python36.dll'
let $PATH = $VIM.'\python-3.6.8;'.$PATH
source $VIM/vimfiles/bundle/wrapuni/wrapuni.vim
map <F5> :call Reddit()<CR>

map <F9> :!!<CR>

let g:airline_powerline_fonts = 1

set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let eq = ''
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      let cmd = '""' . $VIMRUNTIME . '\diff"'
      let eq = '"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
endfunction

