colorscheme xoria256

if has("gui_gtk2")
    :set guifont=Ubuntu\ Mono\ 12
elseif has("x11") " Also for GTK 1
    :set guifont=*-lucidatypewriter-medium-r-normal-*-*-180-*-*-m-*-*
elseif has("gui_win32")
    :set guifont=Consolas:h10
endif

